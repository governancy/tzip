---
All headers are required unless otherwise stated.
tzip: TZIP number
title: TZIP title, with an optional name as a prefix (e.g. 'FA1 - Abstract Ledger')
author: Name(s) of author(s), ideally with their username(s) or email address(es)
gratuity: Optional. A Tezos address controlled by an author capable of receiving gratuities from grateful Tezos users
discussions-to: Optional. A url pointing to the official discussion thread
status: <Work In Progress | Draft | Withdrawn | Submitted | Deprecated | Superseded>
type: Defined in TZIP-2
created: Date created on, format yyyy-mm-dd
requires: Optional. Comma-separated TZIP numbers
replaces: Optional. Comma-separated TZIP numbers
superseded-by: Optional. Comma-separated TZIP numbers
---


## Summary

Provide a simplified and layman-accessible explanation of the TZIP.

## Abstract

A short (200-500 word) but comprehensive description of the issue being
addressed and the proposed solution.

## Motivation

It should clearly explain why the existing implementation is inadequate to
address the problem that the TZIP solves.

## Specification

The technical specification should describe the syntax and semantics of any new
feature.

## Rationale

The rationale fleshes out the specification by describing what motivated the
design and why particular design decisions were made. It should describe
alternate designs that were considered and related work, e.g. how the feature is
supported in other languages. The rationale may also provide evidence of
consensus within the community, and should discuss important objections or
concerns raised during discussion.

## Backwards Compatibility

All TZIPs that introduce backwards incompatibilities or supersede other TZIPs
must include a section describing these incompatibilities, their severity, and
solutions.

## Test Cases

Test cases for an implementation are strongly recommended as are any proofs of
correctness via formal methods.

## Implementations

Any implementation must be completed before the TZIP is given the status
Submitted, but is not mandatory in *Draft* and *Work In Progress* status.

## Appendix

A list of references relevant to the proposal.

## Copyright

All TZIPs must be in the public domain, or a under a permissive license
substantially identical to placement in the public domain. Example of a
copyright waiver:

Copyright and related rights waived via
[CC0](https://creativecommons.org/publicdomain/zero/1.0/).
